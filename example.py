from localization import fault_localization

faulty_path = [1, 2, 3, 4, 5, 7, 9, 10, 15, 16, 17, 18, 19, 16, 17, 18, 19, 34, 0] 
input_path = 'C:/Users/Administrator/Desktop/localization/example'
output_path = 'C:/Users/Administrator/Desktop/localization/example/output'
oracle_file = "reminder_oracle.py"
oracle_funtion = 'reminder_oracle'
file = 'reminder.py'
function_name = 'reminder'
dom_csv_path = 'C:/Users/Administrator/Desktop/localization/example/dom_res/reminder_mcmc_domain.csv'
loc = fault_localization(faulty_path, input_path, file, function_name, oracle_file, oracle_funtion, output_path, dom_csv_path)
# loc.mcdc_info()
# loc.mcdc_localization()
# loc.mutation_localization()
loc.run_localization()