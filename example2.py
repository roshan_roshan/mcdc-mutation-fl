from localization import fault_localization

faulty_path = [1, 2, 3, 4, 5, 6, 7, 10, 13, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 21, 22, 23, 24, 25, 26, 27, 28, 29, 0]
input_path = 'C:/Users/Administrator/Desktop/localization/example'
output_path = 'C:/Users/Administrator/Desktop/localization/example/output'
oracle_file = "expint_oracle.py"
oracle_funtion = 'expint_oracle'
file = 'expint.py'
function_name = 'expint'
dom_csv_path = 'C:/Users/Administrator/Desktop/localization/example/dom_res/expint_mcmc_domain.csv'
loc = fault_localization(faulty_path, input_path, file, function_name, oracle_file, oracle_funtion, output_path, dom_csv_path)
# loc.mcdc_info()
# loc.mcdc_localization()
# loc.mutation_localization()
loc.run_localization()
