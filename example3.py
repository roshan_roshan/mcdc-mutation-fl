from localization import fault_localization

faulty_path = [1, 2, 3, 5, 6, 7, 8, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 16, 17, 18, 19, 20, 22, 16, 23, 0]
input_path = 'C:/Users/Administrator/Desktop/localization/example'
output_path = 'C:/Users/Administrator/Desktop/localization/example/output'
oracle_file = "gammaq_oracle.py"
oracle_funtion = 'gammaq_oracle'
file = 'gammaq.py'
function_name = 'gammaq'
dom_csv_path = 'C:/Users/Administrator/Desktop/localization/example/dom_res/gammaq_mcmc_domain.csv'
loc = fault_localization(faulty_path, input_path, file, function_name, oracle_file, oracle_funtion, output_path, dom_csv_path)
# loc.mcdc_info()
# loc.mcdc_localization()
# loc.mutation_localization()
loc.run_localization()
