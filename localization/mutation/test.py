import os
import csv
from collections import Counter
from collections import defaultdict
import numpy as np
from .ex_mutation_expression import *
from .cfg_feature import cfg_feature
from .ast_feature import ast_feature
from .mutation_analysis import mutation_function


class mutation_priority_list:
    def __init__(self, file, function, input_dir, output_dir, test_data) :
        self.output_dir = output_dir
        self.test_data = test_data
        self.input_dir = input_dir
        self.file = file
        self.function = function

    def generate_mutatnt(self):
        print("this process need several minutes")
        mutants = mutation_function(self.input_dir, self.file, self.function, self.output_dir, self.test_data, 'm')
        mutants.run()
        print("mutant version is ready. please extract script with create_mutated_script() for next usage without using thist method.")

    def read_data(self):
        test_data = []
        with open(f"{self.output_dir}/Mutation_analysis/data.txt", 'r') as f:
            lines = f.readlines()
            for i in lines:
                test_data.append(i[1:-2])
        return test_data

    def read_mutation(self):
        mutated_to = list()
        mutated_from = list()
        lis = os.listdir(f"{self.output_dir}/Mutation_analysis")
        lis.remove('data.txt')
        with open(f"{self.output_dir}/Mutation_analysis/{lis[0]}") as f:
            lines = f.readlines()
            for i in lines:
                if "-" in i[0] and " " in i[1]:
                    mutated_from.append(i)
            for i in lines:
                if "+" in i[0] and " " in i[1]:
                    mutated_to.append(i)
        self.to = mutated_to
        self.fromm = mutated_from
        return self

    def create_mutated_script(self):
        self.goal_lines = list()
        directory = f"{self.output_dir}/mutation_codes"
        if not os.path.exists(directory):
            os.makedirs(directory)
        with open(f"{self.input_dir}/{self.file}", 'r') as source:
            lines = source.readlines()
        count = 0
        for i in self.to:
            count += 1
            ix = i.find(":")
            # print(i[ix-2:ix])
            goal_line = int(i[ix-2:ix])
            self.goal_lines.append(goal_line)
            with open(f"{self.input_dir}/{self.file}", "r") as inf:
                space = inf.readlines()[goal_line-1]
                for num, ka in enumerate(space):
                    if ka != " ":
                        sp = " " * num
                        break
            for numm, kk in enumerate(i[6:]):
                if kk != " ":
                    spp = numm
                    break
            with open(f"{self.output_dir}/mutation_codes/mutation{count}.py", 'w') as f:
                for j in range(0, goal_line - 1):
                    f.write(lines[j])
                f.write(sp + i[6+spp:])
                for k in range(goal_line, len(lines)):
                    f.write(lines[k])
        print("script of mutation versions extracted to output directory")
        return self.goal_lines

    def kill_survive(self):
        test_data = self.read_data()
        test_data_mutant = list()
        mutat = os.listdir(f"{self.output_dir}/Mutation_analysis")
        mutat.remove( 'data.txt')
        for i in range(0, len(mutat)):
            k = [test_data[i]]
            with open(f"{self.output_dir}/Mutation_analysis/mutation_analysis{i}.txt", 'r') as f:
                lines = f.readlines()
                for i in lines[0:-5]:
                    if "killed" in i:
                        k.append("killed")
                    elif "survived" in i:
                        k.append("survived")
                    elif "timeout" in i:
                        k.append("timeout")
            test_data_mutant.append(k)
        return test_data_mutant

