import sys
from unittest import TestCase
import json


class mutation(TestCase):
    def test_mu(self):
        with open('./config.json') as config_file, open("./data.json") as data_file:
            config = json.load(config_file)
            data = json.load(data_file)
            for p in config['config']:
                input_path = p['input_path']
                module_name = p['module_name']
                function_name = p['function_name']
            for d in data["data"]:
                inp = d["input"]
                res = d["result"]
        sys.path.append(f"{input_path}")
        exec(f"from {module_name} import {function_name}")
        res_tdg = eval(f"{function_name}{inp}")
        if type(res_tdg) == tuple:
            self.assertNotEqual(list(res_tdg), res)
        else:
            self.assertNotEqual(res_tdg, res)

