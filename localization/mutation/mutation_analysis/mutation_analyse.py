import json
import csv
import sys
import os


class mutation_function:
    def __init__(self, input_path, module_name, function_name, output_path, test_data, report):
        self.input_path = input_path
        self.modul_name = module_name
        self.function_name = function_name
        self.output_path = output_path
        self.report = report
        self.data = test_data
        self.result = list()
        self.creat_config()
        self.repair_data()
        self.create_result_with_oracle()

    def progress_report(self, count, total, status=''):
        bar_len = 50
        filled_len = int(round(bar_len * count / float(total)))
        percents = round(100.0 * count / float(total), 1)
        bar = '|' * filled_len + '_' * (bar_len - filled_len)
        sys.stdout.write('\r%s %s%s %s' % (bar, percents, '%', status))
        sys.stdout.flush()

    def creat_config(self):
        data = dict()
        data["config"] = list()
        data["config"].append({ "input_path": self.input_path,
                                               "module_name": self.modul_name,
                                               "function_name": self.function_name})
        with open("./localization/mutation/mutation_analysis/config.json", "w") as file:
            json.dump(data, file)

    def repair_data(self):
        r_data = list()
        for i in self.data:
            tt = str()
            tt += '('
            for j in i:
                tt += j + ","
            tt = tt[:-1] + ')'
            r_data.append(tt)
        self.data = r_data
        return self

    def create_result_with_oracle(self):
        sys.path.append(self.input_path)
        exec(f"from {self.modul_name} import {self.function_name}")
        for i in self.data[1:]:
            try:
                res = eval(f"{self.function_name}{i}")
                self.result.append(res)
            except:
                self.result.append(None)
        return self

    def write_to_json(self, innput, result):
        data = dict()
        data["data"] = list()
        data["data"].append({"input": innput, "result": result})
        with open("./localization/mutation/mutation_analysis/data.json", "w") as file:
            json.dump(data, file)

    def delete_json(self):
        os.remove("./localization/mutation/mutation_analysis/config.json")
        os.remove("./localization/mutation/mutation_analysis/data.json")

    def create_directory(self):
        directory = "Mutation_analysis"
        parent_dir = self.output_path
        path = os.path.join(parent_dir, directory)
        os.mkdir(path)

    def run(self):
        count = 0
        rep = ""
        if "y" in self.report:
            # add rep yaml report
            pass
        if "h" in self.report:
            # add rep html report
            pass
        if "m" in self.report:
            rep += " -m"
        try:
            self.create_directory()
        except:
            pass
        # print(len(self.data))
        for i in range(len(self.data)-1):
            count += 1
            self.progress_report(count, len(self.data))
            self.write_to_json(self.data[i], self.result[i])
            now = str(self.data[i])
            os.system(f"echo '{now}' >> {self.output_path}/Mutation_analysis/data.txt")
            os.system(f"cd  ./localization/mutation/mutation_analysis & python mut.py --target {self.modul_name} --unit-test tester_fail {rep} > {self.output_path}/Mutation_analysis/mutation_analysis_fail{i}.txt")
            os.system(f"cd  ./localization/mutation/mutation_analysis & python mut.py --target {self.modul_name} --unit-test tester_pass {rep} > {self.output_path}/Mutation_analysis/mutation_analysis_pass{i}.txt")

        self.delete_json()

