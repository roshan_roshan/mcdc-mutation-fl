import os
import csv
import math
import random
from collections import defaultdict
from .mutation_analysis import mutation_function


class mu_loc:
    def __init__(self, budget, branch, test_datas, input_path, file, function, output_path):
        self.number_test_data = budget
        self.file = file
        self.function = function
        self.input_path = input_path
        self.output_path = output_path
        self.branch = branch
        self.test_data = test_datas
        newTD = list()
        for i in range(budget):
            newTD.append(random.choice(self.test_data))
        self.test_data = newTD

    def generate_mutatnt(self):
        print("this process need several minutes")
        mutants = mutation_function(self.input_path, self.file, self.function, self.output_path, self.test_data, 'm')
        mutants.run()

    def read_data(self):
        test_data = []
        with open(f"{self.output_path}/Mutation_analysis/data.txt", 'r') as f:
            lines = f.readlines()
            for i in lines:
                test_data.append(i[1:-2])
        return test_data

    def kill_survive(self):
        test_data_mutant = defaultdict(list)

        mutat = os.listdir(f"{self.output_path}/Mutation_analysis")
        mutat.remove('data.txt')
        for fil in mutat:
            k = list()
            with open(f"{self.output_path}/Mutation_analysis/{fil}", 'r') as f:
                lines = f.readlines()
                for i in lines[0:-5]:
                    if "killed" in i:
                        if 'fail' in fil:
                            k.append("survived")
                        else:
                            k.append("killed")
                    elif "survived" in i:
                        if 'fail' in fil:
                            k.append('killed')
                        else:
                            k.append("survived")
                    elif "timeout" in i:
                        k.append("timeout")
            test_data_mutant[fil].append(k)
        return test_data_mutant

    def read_mutation(self):
        # output as set of: {mutatnt: [line, 'pass or fail', test_data]}
        test_data = self.read_data()
        kill_survive = self.kill_survive()
        line_mutant = defaultdict(list)
        mutant_info = defaultdict(list)
        lis = os.listdir(f"{self.output_path}/Mutation_analysis")
        lis.remove( 'data.txt')
        for num, k in enumerate(lis):
            count = -1
            with open(f"{self.output_path}/Mutation_analysis/{k}") as f:
                lines = f.readlines()
                for i in lines:
                    if "+" in i[0] and " " in i[1]:
                        count += 1
                        killsurv = kill_survive[k][0][count]
                        # data: passed or failed
                        if 'pass' in k:
                            sign = 'p'
                        else:
                            sign = 'f'
                        ix = i.find(":")
                        goal_line = int(i[ix-2:ix])

                        # extract branch lines result file
                        if goal_line >= self.branch[0] and goal_line <= self.branch[1]:
                            mutant_info[i].append([goal_line, sign, killsurv, test_data[num]])
                            line_mutant[goal_line].append(i)
        return mutant_info, line_mutant

    def ochiai_score(self, score_info):
        mut_score = dict()
        for mut, info in score_info.items():
            try:
                mut_score[mut] = info['kf'] / math.sqrt((info['kf'] + info['nf']) + (info['kf'] + info['kp']))
            except:
                mut_score[mut] = 0
        return mut_score

    def jaccard_score(self, score_info):
        mut_score = dict()
        for mut, info in score_info.items():
            try:
                mut_score[mut] = info['kf'] / info['kf'] + info['nf'] + info['kp']
            except:
                mut_score[mut] = 0
        return mut_score
    
    def op2_score(self, score_info):
        mut_score = dict()
        for mut, info in score_info.items():
            mut_score[mut] = info['kf'] - (info['kp'] / (info['kp'] + info['np'] + 1))
        return mut_score

    def run_loc(self):
        # mut = {mut: [kp, np, kf, nf]}
        score_info = dict()
        self.generate_mutatnt()
        # seprate fail or pass results
        mutant_info, line_mutant = self.read_mutation()

        # if passed then compute: kp, np
        # elif failed then compute: kf, nf
        # for <each mutant> compute kp(test cases that passed program and killed mutant), np(test cases that passed program but not killed mutant), 
        # kf(tst cases that failed and killed mutant), nf (test cases that failed and not killed mutant)
        for mut, info in mutant_info.items():
            score_info[mut] = {'kp': 0, 'np': 0, 'kf': 0, 'nf': 0}
            for i in info:
                if i[1] == 'p' and i[2] == 'killed':
                    score_info[mut]['kp'] += 1
                if i[1] == 'p' and i[2] == 'survived':
                    score_info[mut]['np'] += 1
                if i[1] == 'f' and i[2] == 'killed':
                    score_info[mut]['kf'] += 1
                if i[1] == 'f' and i[2] == 'survived':
                    score_info[mut]['nf'] += 1
        
        # after compute 4 parameters run jassard or ochiai
        mut_score_ochiai = self.ochiai_score(score_info)
        mut_score_jaccard = self.jaccard_score(score_info)
        mut_score_op2 = self.op2_score(score_info)
        return mut_score_ochiai, mut_score_jaccard, mut_score_op2