from .mcdc import mcdc_loc
from .mutation import mu_loc
import sys
import inspect
import csv
from texttable import Texttable


class fault_localization:
    def __init__(self, faulty_path, input_path, file, function_name, oracle_file, oracle_name, output_path, dom_csv_path):
        self.input_domain = self.read_dom_csv(dom_csv_path)
        self.input_path = input_path
        self.output_path = output_path
        self.file = file[:-3]
        self.oracle_file = oracle_file[:-3]
        self.function = function_name
        self.oracle_function = oracle_name
        self.put, self.put_imports, self.extra_code = self.read_code(self.file, self.function)
        self.oracle, self.oracle_imports, _ = self.read_code(self.oracle_file, self.oracle_function)
        self.faulty_path = faulty_path

    def read_code(self, file_name, function_name):
        sys.path.append(self.input_path)
        exec(f"from {file_name} import {function_name}")
        source = inspect.getsource(eval(f"{function_name}"))
        imports = []
        with open(f'{self.input_path}/{file_name}.py', 'r') as f:
            lines = f.readlines()
            for i in lines:
                if "import" in i:
                    imports.append(i)
        ffile = open(self.input_path + "/" + file_name + ".py", "r")
        lines = ffile.readlines()
        extra = lines[self.max_line(file_name, function_name):]
        return source, imports, extra

    def max_line(self, file, func):
        ffile = open(self.input_path + "/" + file + ".py", "r")
        lines = ffile.readlines()
        rang = len(lines)
        name = "def " + str(func)
        min_number = float("inf")
        for number, line in enumerate(lines):
            if name in line:
                min_number = number + 1
            elif ("def" in line and number > min_number - 1):
                max_number = number
                break
            elif number == rang - 1:
                max_number = number + 1
            else:
                continue
        return max_number

    def read_dom_csv(self, file_path):
        test_data = list()
        with open(file_path, 'r') as f:
            reader = csv.reader(f)
            for i in reader:
                test_data.append(i)
        return test_data

    def mcdc_info(self):
        obj_mcdc = mcdc_loc(self.faulty_path, self.input_domain, self.put, self.oracle, self.function, self.put_imports,self.oracle_function, self.oracle_imports, self.extra_code, self.output_path)
        obj_mcdc.mcdc_save_visualize()
        obj_mcdc.extract_decision_graph()
        seq, result = obj_mcdc.mcdc_decision_solver()
        unmapped, cond_str, mappdict = obj_mddc.condition_extractor_string()
        return seq, result

    def mcdc_localization(self):
        budget = 30
        # output: nodes between two condition
        obj_mcdc = mcdc_loc(self.faulty_path, self.input_domain, budget, self.put, self.oracle, self.function, self.put_imports,self.oracle_function, self.oracle_imports, self.extra_code, self.output_path)
        area, area_bound_cond = obj_mcdc.localize()
        return area, area_bound_cond

    def mutation_localization(self, branch):
        budget = 5
        obj_mutation = mu_loc(budget, branch, self.input_domain, self.input_path, self.file, self.function, self.output_path)
        mut_score_ochiai, mut_score_jaccard, mut_score_op2 = obj_mutation.run_loc()
        return mut_score_ochiai, mut_score_jaccard, mut_score_op2


    def run_localization(self):
        branch, branch_conditions = self.mcdc_localization()
        branch = [i + len(self.put_imports) + 1 for i in branch]
        print('\n')
        print(style.CREDBG, f'fault in between branch {branch_conditions} and line of code {branch}', style.RESET)
        print('\n\n')

        mut_score_ochiai, mut_score_jaccard, mut_score_op2 = self.mutation_localization(branch)
        
        print('\n\n')
        t = Texttable()
        t.header(['mutant', 'ochiai', 'jaccard', 'op2'])
        for i in range(len(mut_score_ochiai.items())):
            t.add_row([list(mut_score_ochiai.keys())[i], list(mut_score_ochiai.values())[i],
            list(mut_score_jaccard.values())[i], list(mut_score_op2.values())[i]])
        print(t.draw())

class style():
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    UNDERLINE = '\033[4m'
    RESET = '\033[0m'
    CEND      = '\33[0m'
    CBOLD     = '\33[1m'
    CITALIC   = '\33[3m'
    CURL      = '\33[4m'
    CBLINK    = '\33[5m'
    CBLINK2   = '\33[6m'
    CSELECTED = '\33[7m'
    CBLACK  = '\33[30m'
    CRED    = '\33[31m'
    CGREEN  = '\33[32m'
    CYELLOW = '\33[33m'
    CBLUE   = '\33[34m'
    CVIOLET = '\33[35m'
    CBEIGE  = '\33[36m'
    CWHITE  = '\33[37m'
    CBLACKBG  = '\33[40m'
    CREDBG    = '\33[41m'
    CGREENBG  = '\33[42m'
    CYELLOWBG = '\33[43m'
    CBLUEBG   = '\33[44m'
    CVIOLETBG = '\33[45m'
    CBEIGEBG  = '\33[46m'
    CWHITEBG  = '\33[47m'

    CGREY    = '\33[90m'
    CRED2    = '\33[91m'
    CGREEN2  = '\33[92m'
    CYELLOW2 = '\33[93m'
    CBLUE2   = '\33[94m'
    CVIOLET2 = '\33[95m'
    CBEIGE2  = '\33[96m'
    CWHITE2  = '\33[97m'

    CGREYBG    = '\33[100m'
    CREDBG2    = '\33[101m'
    CGREENBG2  = '\33[102m'
    CYELLOWBG2 = '\33[103m'
    CBLUEBG2   = '\33[104m'
    CVIOLETBG2 = '\33[105m'
    CBEIGEBG2  = '\33[106m'
    CWHITEBG2  = '\33[107m'