import graphviz as gv
from IPython.display import display, Image
import functools
import re
from datetime import datetime
import os
from PIL import Image

digraph = functools.partial(gv.Digraph, format='png')



def add_nodes(graph, nodes):
    for n in nodes:
        if isinstance(n, tuple):
            graph.node(n[0], **n[1])
        else:
            graph.node(n)
    return graph


def add_edges(graph, edges):
    for e in edges:
        if isinstance(e[0], tuple):
            graph.edge(*e[0], **e[1])
        else:
            graph.edge(*e)
    return graph


# add the predicates as nodes to a list
def list_nodes(decision, nodes):
    nodes = ['Start']
    for x in decision:
        if re.match(r"[A-Za-z]", x):
            nodes += x
    nodes += ['End']
    return nodes


def add_connection(connections, a, b):
    # help function to build intermediate graph dictionary
    if a in connections:
        connections[a].append(b)
    else:
        ls = [b]
        connections[a]= ls

def extract_edges(decision, connections):
    # extract the edges from the decision
    # due to the numerous possible orderings of the different predicates, booleans and parenthesis
    # this algorithm contains a lot of different conditionals
    orStack = list()
    andStack = list()
    previous = 'Start'
    sign = ''
    for i, elem in enumerate(decision):
        if elem == '&' or elem == '|':  
            #if sign: set sign
            sign = decision[i]
        elif elem == '(' and sign == '':  
            #if left parenthesis: connect parenthesis to previous element
            s1 = '(' + str(i)
            add_connection(connections, previous, s1)
            previous = s1
        elif re.match(r"[A-Za-z]", elem) and sign == '': 
            #if first condition: connect to previous element
            add_connection(connections, previous, elem)
            previous = elem
        elif re.match(r"[A-Za-z]", elem) and previous[0] == '(':
            # if previous element is left parenthesis: connect to previous element
            add_connection(connections, previous, elem)
            previous = elem
        elif (re.match(r"[A-Za-z]", elem) or elem == '(') and sign == '&':
            # connect to previous condition
            # if previous condition is within parenthesis connect to every condition that is
            # connected via 'or' to the next condition
            # the traverse of the parenthesis (in the direction left) is finished when the
            # left outer parenthesis is reached
            if elem == '(':
                s1 = elem + str(i)
            else:
                s1 = elem
            previous = s1
            if decision[i-1] == '(':
                add_connection(connections, '(' + str(i-1), s1)
            elif re.match(r"[A-Za-z]", decision[i-2]):
                add_connection(connections, decision[i-2], s1)
            else:
                back = 2
                first = True # first step into parenthesis
                while i - back >= 0:
                    if decision[i - back] == ')': 
                        # entering one nesting deeper
                        andStack.append(')')
                    elif re.match(r"[A-Za-z]", decision[i - back]) and first: 
                        # connect to the first condition from the rigt
                        add_connection(connections, decision[i - back], s1)
                        first = False
                    elif re.match(r"[&, |]", decision[i - back]): 
                        #set sign
                        sign = decision[i - back]
                    elif re.match(r"[A-Za-z]", decision[i - back]) and sign == '|': 
                        # connect if connection is 'or'
                        add_connection(connections, decision[i - back], s1)
                    elif decision[i - back] == '(':
                        andStack.pop()
                        if not andStack:
                            break # stop when outer left parenthesis is reached
                    back += 1

        elif (re.match(r"[A-Za-z]", elem) or elem == '(') and sign == '|':
            # traverse the decision backwards till point where connection is adequate, 
            # without paranthesis this means the start
            # with paranthesis this means the left paranthesis
            if elem == '(':
                s1 = elem + str(i)
            else:
                s1 = elem
            previous = s1
            if decision[i-1] == '(':
                add_connection(connections, '(' + str(i-1), s1)
            else:
                back = 2
                while i - back >= 0:
                    if i - back == 0:
                        if decision[0] == '(':
                            connect = '(0'
                        else:
                            connect = 'Start'
                        add_connection(connections, connect, s1)
                    elif decision[i - back] == ')':
                        # entering one nesting deeper
                        orStack.append(')')
                    elif decision[i - back] == '(':
                        if not orStack:
                            add_connection(connections, '(' + str(i - back), s1)
                            break
                        else:
                            orStack.pop()
                    back += 1
    return connections

def sanatize(dic):
    # removes the '(' as key and replaces it with the real connections
    for k, v in dic.items():
        connect(dic, k, k,  v)
    for k in list(dic.keys()):
        if re.match(r"[(]", k) != None:
            del dic[k]
        else:
            dic[k] = list(filter(lambda x: re.match(r"[(]", x) == None, dic[k]))

def connect(dic, start, k, v):
    # connects the appropriate nodes
    if re.match(r"[(]", v[0]) != None:
        connect(dic, start, v[0], dic[v[0]])
        if len(v) > 1:
            connect(dic, start, k, v[1:])
    elif len(v) > 1:
        connect(dic, start, k, v[1:])
        connect(dic, start, k , v[0])
    elif v[0] not in dic[start]:
        dic[start].append(v[0])

def create_decision_graph(decision, path):
    g1 = digraph()
    decision = list(decision)
    nodes = list()
    connections= {}

    sanatize(extract_edges(decision, connections))
    nodes = list_nodes(decision, nodes)
    edges = list()

    for k, v in connections.items():
        for x in v:
            edges.append((k, x))

    for node in nodes:
        if node not in connections.keys() and node != 'End':
            edges.append((node, 'End'))
    add_edges(g1, edges)
    return g1


class Condition:
    def __init__(self):
        self.name = ''
        self.precop = ''
        self.trailop = ''

class Testcase:
    def __init__(self, seq, res):
        self.seq = seq
        self.res = res

class Decision:
    def __init__(self):
        self.conditions = list()
        self.testcases = list()
        
    def extract(self, raw):
        elem = list(raw)
        size = len(raw)
        i = 0
        while i < size:
            temp = Condition()
            temp.name = elem[i]
            if i == 0:
                temp.precop = '-'
            else:
                temp.precop = elem[i-1]
            if (i+1) == size:
                temp.trailop = '-'
            else:
                temp.trailop = elem[i+1]
            self.conditions.append(temp)
            i += 2
    
    def derivation(self):
        length = len(self.conditions)
        self.testcases = [None] * length
        testcase = list()
        for i in range(0, len(self.conditions)):
            for x in range(0, length - (length - i)):
                if self.conditions[x].trailop == "&":
                    testcase.append("1")
                else:
                    testcase.append("0")
            testcase.append("1")
            for x in range(i+1, length):
                if self.conditions[x].precop == "&":
                    testcase.append("1")
                else:
                    testcase.append("0")
            self.testcases[i] = Testcase(''.join(testcase), "True")
            testcase[i]=("0")
            self.testcases.append(Testcase(''.join(testcase), "False"))
            testcase.clear()
    
    def reduce(self):
        li = list()
        temp = list()
        for testcase in self.testcases:
            if testcase.seq not in li:
                li.append(testcase.seq)
                temp.append(testcase)
        self.testcases.clear()
        self.testcases = temp
        
    def print_testcases(self):
        i = 1
        for x in self.testcases:
            print(x.seq)
            print('Testcase ' + str(i) + ': ' + x.seq + '   ' + x.res)
            i += 1

    def write_file(self, fileName):

        with open(fileName, mode='w', encoding='utf8') as resFile:
            i = 1
            conditions = ''
            for cond in self.conditions:
                conditions += cond.name
            resFile.write("             " + conditions + '\n')
            for x in self.testcases:
                resFile.write('Testcase {0:>2d}: {1}  {2}\n'.format(i, x.seq, x.res))
                i += 1


class DecisionError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


def proof_decision(sequence):
    try:
        if len(sequence) == 0:
            raise DecisionError('No conditions')
        
        if len(sequence) == 1:
            if re.match(r"[^A-Za-z]", sequence):
                raise DecisionError('No conditions')

        if re.match(r"^[^A-Za-z(]", sequence):
            raise DecisionError('Wrong start character')
        
        if re.match(r'.+[^A-Za-z)]$', sequence):
            raise DecisionError('Wrong termination character')
        
        if re.match(r".*\([^A-Za-z(]", sequence):
            raise DecisionError('Wrong "(" sequence')

        if re.match(r".+\)[^)&|]", sequence):
            raise DecisionError('Wrong ")" sequence')
        
        if re.match(r".*[A-Za-z][A-Za-z]", sequence):
            raise DecisionError('Condition condition sequence')
        
        if re.match(r".+[\s]+", sequence):
            raise DecisionError('Blanks')
        
        if re.match(r".+[^A-Za-z()&|]", sequence):
            raise DecisionError('Illegal character')
                                
        seq = re.sub('[\(\)]', '', sequence)
        seq_split = re.split("\||\&", seq)
        if len(seq_split) != len(set(seq_split)):
            raise DecisionError('Duplicate letters')
 
        parentheses_check(sequence)
        
    except DecisionError as e:
        if e.value == 'No conditions':
            print('Decision has to contain at least one condition!')
        if e.value == 'Wrong start character':
            print('The decision has to start with a letter or "("')
        if e.value == 'Wrong termination character':
            print('The decision has to en with a letter or ")"')
        if e.value == 'Wrong "(" sequence':
            print('The "(" sign has to be followed by a letter or "("')
        if e.value == 'Wrong ")" sequence':
            print('The ")" sign has to be followed by a "&" or "|" or ")" or should end the decision')
        if e.value == 'Condition condition sequence':
            print('The decision contains two conditions that are not seperated by "&" or "|"')
        if e.value == 'Blanks':
            print('The decision contains white spaces')
        if e.value == 'Illegal character':
            print('The condition should only contains letters and "&", "|", "(", ")"')
        if e.value == 'Duplicate letters':
            print('A letter can only be used once within a decision')
        if e.value == 'Wrong parentheses sequence':
            print('The sequence of parentheses is not correct')
        print("The sequence was: " + sequence)
        raise
    
#check on the right sequence of parentheses
def parentheses_check(sequence):
    stack = list()
    paren = list(filter((lambda x: re.match(r"[()]", x) != None), list(sequence)))
    for parenthesis in paren:
        if parenthesis == "(":
            stack.append(parenthesis)
        elif not stack:
            raise DecisionError('Wrong parentheses sequence')
            break
        else:
            stack.pop()
    if stack:
        raise DecisionError('Wrong parentheses sequence')


def solve_decision(sequence):
    # strip the parentheses of the sequence    
    sequence = re.sub('[()]', '', sequence)
    decision = Decision()
    decision.extract(sequence)
    decision.derivation()
    decision.reduce()
    return decision


def solve_and_visualize(sequence, path):
    seq = sequence
    #check correctness of decision
    # proof_decision(seq)
    
    #create decision graph
    dg = digraph()
    dg = create_decision_graph(seq, path)
    
    #solve decision
    decision = solve_decision(seq)
    
    #create directory with timestamp
    # now = datetime.now()
    # stamp = re.sub('[-\.: ]', '', str(now))
    # os.mkdir(path)
    
    #save results
    decision.write_file(path + '/res.txt')
    
    #visualize solution
    nodes = list()
    for x in seq:
        if re.match(r"[a-z, A-Z]", x):
            nodes.append(x)

    for a, test in enumerate(decision.testcases):
        for i, elem in enumerate(test.seq):
            if elem == '0':
                paint = 'red'
            else:
                paint = 'green'
            dg.node(nodes[i], color = paint, style = 'filled')
        if test.res == 'True':
            paint = 'green'
        else:
            paint = 'red'
        dg.node("End", color = paint, style = 'filled')
        dg.render(path + '/dg' + str(a))

    for x, elem in enumerate(decision.testcases):
        im = Image.open(path + '/dg' + str(x)+ '.png')
        im.show()
        elem.seq                


class mcdc_tools:
    
    def __init__(self,sequence, path):
        self.path = path
        self.sequence = sequence

    def decision_graph(self):
        g1 = create_decision_graph(self.sequence, self.path)
        g1.format = 'png'
        g1.render(f'{self.path}/decision_graph/Graph', view = True)

    def decision_solver(self):
        sequence = list()
        result = list()
        solved = solve_decision(self.sequence)
        for i in solved.testcases:
            sequence.append(i.seq)
            result.append(i.res)
        return sequence, result

    def save_and_visualize(self):
        path = self.path
        solve_and_visualize(self.sequence, path)