# method:
#   step1. minize condition to find effective condition that cover all of path (result extract k condition)
#   step2: faulty branch in minimized conditions
#          step2.1: run code with test data from domain points
#          step2.2: if result is not equal with oracle then
#          step2.3: convert k`th condition to negative 
#          step2.4: if result of this execution not equal to oracle then fault happened in upper part of code
#                    2.4.1: then conver k-1`th condition to negative and repeat the process until step2.5 happen
#          step2.5: if result  is equal with oracle then the faulty part is in below section of condition while reach to next condition


import ast
import astor
import random
import time
import sys
import glob
import shutil
import os
from .condition_tools import CompareVisitor
from .mcdc_tools import mcdc_tools

    
class mcdc_loc:
    def __init__(self, path, test_datas, budget, code, oracle_code, code_name, code_imports, oracle_name, oracle_imoports, extra, output_path):
        self.path = path
        self.treshould = budget
        self.output_path = output_path
        self.test_data = test_datas
        self.code = code
        self.oralce_code = oracle_code
        self.code_name = code_name
        self.oracle_name = oracle_name
        self.code_imports = code_imports
        self.oracle_imoports = oracle_imoports
        self.extra_code = extra

    def condition_extractor(self):
        visitor = CompareVisitor(self.path)
        tree = ast.parse(self.code)
        visitor.visit(tree)
        path_condition, path_condition_lineno = visitor.return_path_conditions()
        if len(self.code_imports) != 0:
            for i, j in path_condition_lineno.items():
                path_condition_lineno[i] = j + len(self.code_imports) + 1
        print(path_condition_lineno)
        return path_condition, path_condition_lineno

    def condition_extractor_string(self):
        visitor = CompareVisitor(self.path)
        tree = ast.parse(self.code)
        visitor.visit(tree)
        return unmapped, cond_str, mappdict

    def extract_decision_graph(self):
        cond_string = self.condition_extractor_string()
        graph = mcdc_tools(cond_string, self.output_path)
        graph.decision_graph()
    
    def mcdc_decision_solver(self):
        unmapped, cond_str, mappdict = self.condition_extractor_string()
        graph = mcdc_tools(cond_str, self.output_path)
        seq, res = graph.decision_solver()
        return seq, res
    
    def mcdc_save_visualize(self):
        unmapped, cond_str, mappdict = self.condition_extractor_string()
        graph = mcdc_tools(cond_str, self.output_path)
        graph.save_and_visualize()

    def localize(self):
        ans = list()
        cond_ans = list()
        path_condition, path_condition_lineno = self.condition_extractor()
        path_condition_lineno = {k: v for k, v in sorted(path_condition_lineno.items(), key=lambda item: item[1], reverse=True)}
        p_condition = path_condition_lineno.keys()

        print(style.RED, "path conditions:", p_condition, "\n", style.RESET)
        # for all condition in path reverse condition one by one
        num = 0
        for cond in p_condition:
            num = num + 1
            print(style.MAGENTA, "reversed condition:  ", cond, style.RESET)
            code, oraclde_code = change_imports(self.code, self.oralce_code, self.code_imports, self.oracle_imoports, self.extra_code)
            
            #   create oracle reverse, faulty reverse
            reversed_code, reversed_oracle = change_code(num, self.code, self.oralce_code, self.code_imports, self.oracle_imoports, self.code_name, self.oracle_name, 
                cond, p_condition, path_condition_lineno, self.output_path)
            
            # save faulty reverse files
            save_codes(num, code, reversed_code, oraclde_code, reversed_oracle)
            time.sleep(1)

            t = 0
            while t < self.treshould:
                t += 1
                flag = False
                # select test data
                data = random.choice(self.test_data)

                # run oracle code test data
                results = execute_function(num, data, self.code_name, self.oracle_name)
                
                # first compare oracle and faulty code
                # if result not equal: go to the next condition and check for reverse resutls
                # elif result of oracle and faulty code is equal then check other test data(the test data number need threshould)     
                if results['fcode'] != results['oracle']:
                    # compare reversed oracle and reversed faulty
                    # if not equal then fault location is in upeer lines of code condition and need to check the above condition and need to go upper condition
                    # elif result of oracle and reversed faulty are equal then fault location is in lower lines of code from condition
                    
                    if results['reverse_fcode'] != results['reverse_oracle']:
                        flag = False
                    else:
                        flag = True
                    break

            if flag is True:
                cond_ans.append(cond)
                ans.append(path_condition_lineno[cond])
                break   
        try:
            line_no = list(path_condition_lineno.values())
            ans.append(line_no[line_no.index(ans[0]) - 1])

            for i, j in path_condition_lineno.items():
                if j == line_no[line_no.index(ans[0]) - 1]:
                    cond_ans.append(i)
            # delete and move file from codes directori
            if ans[0] > ans[1]:
                tmp = ans[0]
                tmp1 = cond_ans[0]
                ans[0] = ans[1]
                ans[1] = tmp
                cond_ans[0] = cond_ans[1]
                cond_ans[1] = tmp
        except:
            print('MCDC cant detect')
            ans = [0, len(self.code.splitlines())]
        delete_files()
        return ans, cond_ans


def change_imports(code, oracle, code_imports, oracle_imports, extra_code):
    ims = str()
    for i in code_imports:
        ims = i + "\n"
    code = ims + code

    ims = str()
    for i in oracle_imports:
        ims = i + "\n"
    oracle = ims + oracle

    for i in extra_code:
        code += i
        oracle += i
    return code, oracle

def change_code(num, code, oracle, code_imports, oracle_imoports, code_name, oracle_name, condition, path_condition, path_condition_lineno, output_path):
    old = condition
    change_line_no = path_condition_lineno[condition]

    if old[0] == '(':
        old = old[1:-1]
    new = "not(" + old + ")"
    new_code = ''
    lines = code.splitlines()
    print(change_line_no)
    if len(code_imports) != 0:
        #print(lines[change_line_no - len(code_imports) -2])
        #print(old)
       #print(new)
        for i in range(change_line_no - len(code_imports) - 2):
            new_code = new_code + lines[i] + "\n"

        subsituation = lines[change_line_no - len(code_imports) -2].replace(old, new)
        #print(subsituation)
        new_code = new_code + subsituation + "\n"

        for i in range(change_line_no - len(code_imports) - 1, len(lines)):
            new_code = new_code + lines[i] + "\n"
    else:
        for i in range(change_line_no - 1):
            new_code = new_code + lines[i] + "\n"
        subsituation = str(lines[change_line_no - 1]).replace(str(old), str(new))

        new_code = new_code + subsituation + "\n"
        for i in range(change_line_no, len(lines)):
            new_code = new_code + lines[i] + "\n"
    ims = str()
    for i in code_imports:
        ims = i + "\n"
    new_code = ims + new_code

    new_oracle = ''
    lines = oracle.splitlines()
    for i in range(change_line_no-1):
        new_oracle = new_oracle + lines[i] + "\n"

    subsituation = lines[change_line_no-1].replace(old, new)
    new_oracle = new_oracle + subsituation + "\n"

    for i in range(change_line_no, len(lines)):
        new_oracle = new_oracle + lines[i] + "\n"
    
    ims = str()
    for i in oracle_imoports:
        ims = i + "\n"
    new_oracle = ims + new_oracle

    new_code = new_code.replace(code_name, f"{code_name}_reverse{num}")
    new_oracle = new_oracle.replace(oracle_name, f"{oracle_name}_reverse{num}")
    # print(new_code)
    return new_code, new_oracle


def save_codes(num, code, reversed_code, oracle_code, reversed_oracle):

    with open('./localization/codes/fcode.py', 'w') as f:
        f.writelines(code)
    with open(f'./localization/codes/reverse_fcode{num}.py', 'w') as f:
        f.writelines(reversed_code)
    with open('./localization/codes/oracle.py', 'w') as f:
        f.writelines(oracle_code)
    with open(f'./localization/codes/reverse_oracle{num}.py', 'w') as f:
        f.writelines(reversed_oracle)   

def execute_function(num, test_data, code_name, oracle_name):
    print(style.CYAN, "test data:  ", test_data, style.RESET, "\n")
    res = dict()
    sys.path.append("./localization/codes")
    code_arg = '('
    for i in test_data:
        code_arg = code_arg + str(i) + ","
    code_arg = code_arg[:-1] + ")"
    exec(f"from fcode import {code_name}")
    exec(f"from oracle import {oracle_name}")
    exec(f"from reverse_oracle{num} import {oracle_name}_reverse{num}")
    exec(f"from reverse_fcode{num} import {code_name}_reverse{num}")



    exe1 = code_name + code_arg
    try:
        res["fcode"] = eval(exe1)
    except:
        res["fcode"] = None

    exe2 = code_name + f"_reverse{num}" + code_arg
    try:
        res["reverse_fcode"] = eval(exe2)
    except:
        res["reverse_fcode"] = None

    exe3 = oracle_name + code_arg

    try:
        res["oracle"] = eval(exe3)
    except:
        res["oracle"] = None

    exe4 = oracle_name + f"_reverse{num}" + code_arg
    try:
        res["reverse_oracle"] = eval(exe4)
    except:
        res["reverse_oracle"] = None

    for code, result in res.items():
        print(style.WHITE, f"result of {code} is:   {result}")
    print("\n")
    return res

def delete_files():
    shutil.rmtree('./localization/codes/__pycache__')
    path = "./localization/codes/*" 
    files = glob.glob(path)
    for f in files:
        os.remove(f)

class style():
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    UNDERLINE = '\033[4m'
    RESET = '\033[0m'