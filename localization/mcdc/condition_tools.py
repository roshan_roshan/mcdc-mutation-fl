import ast
import astor
import string


class CompareVisitor(ast.NodeVisitor):
    def __init__(self, path):
        self.record_Bool = list()
        self.record_compare = list()
        self.comp_lineno = dict()
        self.path = path

    def visit_Compare(self, node):
        self.record_compare.append(astor.to_source(node)[0:-1])
        self.comp_lineno[astor.to_source(node)[0:-1]] = node.lineno
        ast.NodeVisitor.generic_visit(self, node)
    
    def visit_BoolOp(self, node):
        flag = False
        for i in self.record_Bool:
            if astor.to_source(node)[1:-2] in i:
                flag = True
        if flag is False:
            self.record_Bool.append(astor.to_source(node)[1:-2])
            self.comp_lineno[astor.to_source(node)[1:-2]] = node.lineno
        ast.NodeVisitor.generic_visit(self, node)

    def return_conditions(self):
        cond_lineno = dict()
        new = list()
        for i in self.record_compare:
            flag = False
            for j in self.record_Bool:
                if i in j:
                    flag = True
            if flag is False and i not in new:
                new.append(i)
        self.record_Bool.extend(new)
        for i in self.record_Bool:
            cond_lineno[i] = self.comp_lineno[i]
        return self.record_Bool, cond_lineno


    def return_path_conditions(self):
        condition, condition_lineno = self.return_conditions()
        path_codition = list()
        path_condition_lineno = dict()
        for i in condition:
            if condition_lineno[i] in self.path:
                path_condition_lineno[i] = condition_lineno[i]
                path_codition.append(i)
        return path_codition, path_condition_lineno

    def return_condition_string_format(self):
        condition, condition_lineno = self.return_conditions()
        cond_str = ''
        for i in condition:
            if condition_lineno[i] in self.path:
                cond_str += f'{i}'
                cond_str += ' and '
        cond_str = cond_str[0:-5] 
        cond_str = cond_str.replace(' and ', "&")
        cond_str = cond_str.replace(' or ', "|")
        ans = list(string.ascii_uppercase)
        map_dictionary = dict()
        count = -1
        record = ''
        cond_str = cond_str.replace(' ', '')
        for i in cond_str:
            if i not in ['|', '&']:
                record += i
            else:
                count += 1
                if record[0] == '(' and record[-1] == ')':
                    map_dictionary[record[1:-1]] = ans[count]
                elif record[0] == '(':
                    map_dictionary[record[1:]] = ans[count]
                elif record[-1] == ')':
                    map_dictionary[record[:-1]] = ans[count]
                else:
                    map_dictionary[record] = ans[count]

                record = ''
        map_dictionary[record] = ans[count+1]
        unmapped = cond_str
        for cond, upper in map_dictionary.items():
            cond_str = cond_str.replace(cond, upper)
        return unmapped, cond_str, map_dictionary
